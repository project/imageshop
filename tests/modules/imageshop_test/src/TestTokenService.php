<?php

namespace Drupal\imageshop_test;

use Drupal\imageshop\ImageShopTempToken;
use Drupal\imageshop\TokenService;

/**
 * Test class to use in tests.
 */
class TestTokenService extends TokenService {

  /**
   * {@inheritdoc}
   */
  public function getTemporaryToken() {
    return new ImageShopTempToken(uniqid());
  }

}
