;(function($, drupalSettings) {
  'use strict';
  var TRIGGER_SELECTOR = '.imageshop-trigger'
  var PROCESSED_CLASS = 'imageshop-processed'
  var WIDGET_SELECTOR = '.imageshop-widget';
  var MANAGED_FILE_WRAPPER_SELECTOR = '.form-managed-file';
  var WRAPPER_EL_SELECTOR = '#media-library-add-form-wrapper';
  var $currentEl;

  function listener(event) {
    var eventdata = event.data.split(";");
    $currentEl.val(eventdata[0]);
    $(".imageshow-closer-close").click();
    // Click the upload button.
    $currentEl.closest(MANAGED_FILE_WRAPPER_SELECTOR).find('.js-upload-button').trigger('mousedown');
  }

  // This part is copy pasted from the documentation.
  function addEvent(event, elem, func) {
    if (elem.addEventListener)  // W3C DOM
      elem.addEventListener(event, func, false);
    else if (elem.attachEvent) { // IE DOM
      elem.attachEvent("on" + event, func);
    }
    else { // No much to do
      elem[event] = func;
    }
  }

  addEvent("message", window, listener);

  function handleBrowseClick(e) {
    var $el = $(this);
    $currentEl = $el.parent().find(WIDGET_SELECTOR);
    e.preventDefault();
    // This should trigger ajax, and show the iframe thing we want.
    $el.closest(MANAGED_FILE_WRAPPER_SELECTOR).find(TRIGGER_SELECTOR).click();
    return false;
  }

  function processImageShopEl(settings, i, n) {
    var $el = $(n);
    if ($el.hasClass(PROCESSED_CLASS)) {
      return;
    }
    $el.addClass(PROCESSED_CLASS);
    var $trigger = $el.closest(MANAGED_FILE_WRAPPER_SELECTOR).find(TRIGGER_SELECTOR);
    $trigger.parent().show();
    $trigger.click(function(e) {
      e.preventDefault();
      $currentEl = $el;
      window.open($trigger.attr('href'), 'imageshop', "width=950, height=650, scrollbars=1, inline=1");
      return false;
    })
    // Remove the whole library, if indicated.
    if (settings.imageShop && settings.imageShop.hideLibrary) {
      var $view = $el.closest(WRAPPER_EL_SELECTOR).next();
      $view.hide();
      // Wait until we trigger a change.
      $view.find('input[type="checkbox"]').on('change', function(e) {
        var selected = $(this).prop('checked');
        if (!selected) {
          return;
        }
        // Let's just press the button and say we are pleased with the result.
        $view.closest('.media-library-widget-modal').css('opacity', .2);
        closeModal();
      });
      // If there is only the one image. Just use that.
      var $checked = $view.find('input[type="checkbox"]:checked');
      if ($checked.length) {
        closeModal();
      }
    }
  }

  function closeModal() {
    // We need this to happen async, to make sure all other JS magic in the
    // library also is good.
    setTimeout(function() {
      var $button = $('.ui-dialog-buttonpane button.button--primary');
      $button.click();
    }, 10);
  }

  Drupal.behaviors.imageShop = {
    attach: function(context, settings) {
      $(context).find(WIDGET_SELECTOR).each(processImageShopEl.bind(null, settings));
    }
  }
})(jQuery, Drupal, drupalSettings)
