<?php

namespace Drupal\imageshop_test\Controller;

use Drupal\Core\Url;
use Drupal\imageshop\Controller\ImageshopController;

/**
 * Returns responses for Imageshop testing routes.
 */
class ImageshopTestController extends ImageshopController {

  /**
   * Builds the response.
   */
  public function build() {
    $uri = Url::fromUserInput('/core/misc/druplicon.png')->setAbsolute()->toString();
    $render = [
      '#template' => '<script>window.parent.postMessage("' . $uri . '");</script>',
      '#type' => 'inline_template',
      '#attached' => [
        'library' => [
          'core/drupalSettings',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    return $this->bareHtmlRenderer->renderBarePage($render, 'Select image', 'markup', $render);
  }

}
