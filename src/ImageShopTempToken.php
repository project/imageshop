<?php

namespace Drupal\imageshop;

/**
 * Temp token value object.
 */
class ImageShopTempToken {

  /**
   * The actual token.
   *
   * @var string
   */
  protected $token;

  /**
   * Timestamp the token was created.
   *
   * @var int
   */
  protected $created;

  /**
   * ImageShopTempToken constructor.
   *
   * @param string $token
   *   The temp token.
   */
  public function __construct($token) {
    $this->token = $token;
    $this->created = time();
  }

  /**
   * Check if a token is expired.
   */
  public function isExpired() {
    return $this->created < time() - (3600 * 24);
  }

  /**
   * Gets the actual token.
   */
  public function getToken() {
    return $this->token;
  }

}
