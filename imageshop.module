<?php

/**
 * @file
 * Drupal hooks for imageshop.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Element\ManagedFile;
use Drupal\imageshop\Element\ImageShopElement;
use Drupal\media_library\MediaLibraryState;

/**
 * Implements hook_theme().
 */
function imageshop_theme($existing, $type, $theme, $path) {
  return [
    'imageshop_iframe' => [
      'variables' => [],
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function imageshop_form_media_library_add_form_upload_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\media_library\MediaLibraryState $state */
  $state = $form_state->get('media_library_state');
  if (!$state instanceof MediaLibraryState) {
    return;
  }
  $type = $state->get('media_library_selected_type');
  // Check what kind of setup we have for this media type.
  $config = \Drupal::config('imageshop.settings')->get('enabled_media_types');
  if (empty($config)) {
    $config = [];
  }
  if (!in_array($type, $config)) {
    return;
  }
  if (empty($form["container"]["upload"]["#process"])) {
    return;
  }
  $form["container"]["upload"]["#type"] = 'imageshop';
  // We need to set the values before the media module does.
  array_unshift($form["container"]["upload"]["#process"], [
    ImageShopElement::class,
    'validateMediaUploadElement',
  ]);
  foreach ($form["container"]["upload"]["#process"] as $delta => $process) {
    if (!is_array($process)) {
      continue;
    }
    if (empty($process[0])) {
      continue;
    }
    if ($process[0] !== ManagedFile::class) {
      continue;
    }
    // Replace the processor with our own, since it's hardcoded to ManagedFile.
    $form["container"]["upload"]["#process"][$delta][0] = ImageShopElement::class;
  }
}

/**
 * Implements hook_cron().
 */
function imageshop_cron() {
  /** @var \Drupal\imageshop\TokenService $token_service */
  $token_service = \Drupal::service('imageshop.token_service');
  $token_service->regenerateToken();
}
