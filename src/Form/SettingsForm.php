<?php

namespace Drupal\imageshop\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Imageshop settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $class = parent::create($container);
    $class->entityTypeManager = $container->get('entity_type.manager');
    return $class;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imageshop_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['imageshop.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#default_value' => $this->config('imageshop.settings')->get('token'),
      '#required' => TRUE,
    ];
    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key'),
      '#default_value' => $this->config('imageshop.settings')->get('private_key'),
    ];
    $form['hide_media_browser'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Drupal media library on imageshop enabled media'),
      '#default_value' => $this->config('imageshop.settings')->get('hide_media_browser'),
    ];
    try {
      /** @var \Drupal\media\Entity\MediaType[] $media_types */
      $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
      $options = [];
      foreach ($media_types as $type) {
        $options[$type->id()] = $type->label();
      }
      $default_value = $this->config('imageshop.settings')->get('enabled_media_types');
      $form['enabled_media_types'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Enabled media types'),
        '#options' => $options,
        '#default_value' => $default_value ? $default_value : [],
      ];
    }
    catch (PluginNotFoundException $e) {
      // Totally fine. Just means we can not choose media types. Probably
      // because we do not have those modules enabled. Which is totally fine.
    }
    $form['imageshop_browser_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings for imageshop browser'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $settings = self::createBrowserSettings($this->config('imageshop.settings')->get('imageshop_browser_settings'));
    $default_settings = self::getDefaultBrowserSettings();
    $form['imageshop_browser_settings']['interface_name'] = [
      '#title' => $this->t('Interface name'),
      '#type' => 'textfield',
      '#default_value' => $settings['interface_name'],
      '#description' => $this->t('The default setting for this is @default', [
        '@default' => $default_settings['interface_name'],
      ]),
    ];
    $form['imageshop_browser_settings']['Language'] = [
      '#title' => $this->t('Language'),
      '#type' => 'select',
      '#options' => [
        'nb-NO' => $this->t('Norwegian'),
        'en-US' => $this->t('English (US)'),
      ],
      '#default_value' => $settings['culture'],
      '#description' => $this->t('The default setting for this is @default', [
        '@default' => $default_settings['culture'],
      ]),
    ];
    $form['imageshop_browser_settings']['show_size_dialog'] = [
      '#title' => $this->t('Show size dialog'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_size_dialog'] === 'true',
      '#description' => $this->t('The default setting for this is @default', [
        '@default' => $default_settings['show_size_dialog'],
      ]),
    ];
    $form['imageshop_browser_settings']['show_crop_dialog'] = [
      '#title' => $this->t('Show crop dialog'),
      '#type' => 'checkbox',
      '#default_value' => $settings['show_crop_dialog'] === 'true',
      '#description' => $this->t('The default setting for this is @default', [
        '@default' => $default_settings['show_crop_dialog'],
      ]),
    ];
    $form['imageshop_browser_settings']['free_crop'] = [
      '#title' => $this->t('Free crop'),
      '#type' => 'checkbox',
      '#default_value' => $settings['free_crop'] === 'true',
      '#description' => $this->t('The default setting for this is @default', [
        '@default' => $default_settings['free_crop'],
      ]),
    ];
    $form['imageshop_browser_settings']['insert_immediately'] = [
      '#title' => $this->t('Insert immediately'),
      '#type' => 'checkbox',
      '#default_value' => $settings['insert_immediately'] === 'true',
      '#description' => $this->t('The default setting for this is @default', [
        '@default' => $default_settings['insert_immediately'],
      ]),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Creates all of the options needed, whether they are set or not.
   */
  public static function createBrowserSettings($settings) {
    if (empty($settings)) {
      $settings = [];
    }
    // Make extra sure we have an array.
    if (!is_array($settings)) {
      $settings = [];
    }
    // Remove empty values.
    $settings = array_filter($settings);
    $default_settings = self::getDefaultBrowserSettings();
    return array_merge($default_settings, $settings);
  }

  /**
   * Gets the default settings for this module.
   */
  public static function getDefaultBrowserSettings() {
    return [
      'culture' => 'en-US',
      'interface_name' => 'screentek',
      'show_size_dialog' => 'false',
      'show_crop_dialog' => 'false',
      'insert_immediately' => 'false',
      'free_crop' => 'true',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->getValue('imageshop_browser_settings');
    // Convert some values to the string true/false.
    $true_false_names = [
      'show_size_dialog',
      'show_crop_dialog',
      'insert_immediately',
      'free_crop',
    ];
    foreach ($settings as $setting => $value) {
      if (!in_array($setting, $true_false_names)) {
        continue;
      }
      $settings[$setting] = 'false';
      if ((bool) $value) {
        $settings[$setting] = 'true';
      }
    }
    $this->config('imageshop.settings')
      ->set('token', $form_state->getValue('token'))
      ->set('private_key', $form_state->getValue('private_key'))
      ->set('hide_media_browser', $form_state->getValue('hide_media_browser'))
      ->set('enabled_media_types', $form_state->getValue('enabled_media_types'))
      ->set('imageshop_browser_settings', $settings)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
