<?php

namespace Drupal\Tests\imageshop\Functional;

use Drupal\Core\Url;
use Drupal\Tests\file\Functional\FileFieldTestBase;
use Drupal\Tests\image\Kernel\ImageFieldCreationTrait;

/**
 * Test basic things with the element.
 *
 * @group imageshop
 */
class ElementTest extends FileFieldTestBase {
  use ImageFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'imageshop',
  ];

  /**
   * Test that an uploaded URL ends up in the right place.
   */
  public function testFileForUri() {
    $field_name = 'my_imageshop_field';
    $type = 'article';
    $entity_type = 'node';
    $this->createImageField($field_name, $entity_type, $type);
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay($entity_type, $type)
      ->setComponent($field_name, [
        'type' => 'imageshop_widget',
        'settings' => [],
      ])
      ->save();
    $this->drupalGet('/node/add/article');
    $file_storage = $this->container->get('entity_type.manager')->getStorage('file');
    $files = $file_storage->loadByProperties([]);
    $this->assertEmpty($files);
    $this->drupalGet('/node/add/article');
    $uri = Url::fromUserInput('/core/misc/druplicon.png')->setOption('absolute', TRUE)->toString();
    $this->submitForm([
      'my_imageshop_field[0][url]' => $uri,
    ], 'Upload');
    $files = $file_storage->loadByProperties([]);
    $this->assertNotEmpty($files);
  }

}
