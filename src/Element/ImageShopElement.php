<?php

namespace Drupal\imageshop\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Element\ManagedFile;
use Drupal\file\FileRepositoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Provides an AJAX/progress aware widget for uploading and saving a file.
 *
 * @FormElement("imageshop")
 */
class ImageShopElement extends ManagedFile {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#process'][] = [
      self::class,
      'processManagedFile',
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function processManagedFile(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = parent::processManagedFile($element, $form_state, $complete_form);
    $element['#attached'] = [
      'library' => [
        'imageshop/imageshop',
      ],
    ];

    if (empty($element['#files'])) {
      array_unshift($element["upload_button"]["#submit"], [
        self::class,
        'submitText',
      ]);
      $element["upload_button"]["#attributes"]["class"][] = 'js-upload-button';
      $element["upload"]['#prefix'] = '<div class="hidden">';
      $element["upload"]['#suffix'] = '</div>';
      // Instead insert a textfield.
      $element['url'] = [
        '#type' => 'textfield',
        '#prefix' => '<div class="hidden">',
        '#suffix' => '</div>',
        '#title' => 'URL',
        '#attributes' => [
          'class' => [
            'imageshop-widget',
            'hidden',
          ],
        ],
      ];
      $config = \Drupal::config('imageshop.settings');
      $element['link'] = [
        '#attached' => [
          'library' => [
            'core/drupal.ajax',
          ],
          'drupalSettings' => [
            'imageShop' => [
              'hideLibrary' => (bool) $config->get('hide_media_browser'),
            ],
          ],
        ],
        '#type' => 'link',
        '#url' => Url::fromRoute('imageshop.iframe', []),
        '#title' => t('Browse imageshop library'),
        '#attributes' => [
          'class' => [
            'button',
            'imageshop-trigger',
          ],
        ],
        '#access' => \Drupal::currentUser()->hasPermission('access imageshop'),
      ];
    }

    return $element;
  }

  /**
   * Helper.
   */
  public static function isRemoveButton($button) {
    try {
      $parents = $button['#array_parents'];
      $button_key = array_pop($parents);
      return $button_key === 'remove_button';
    }
    catch (\Throwable $e) {
      return FALSE;
    }
  }

  /**
   * Validates the upload element.
   */
  public static function validateMediaUploadElement(array $element, FormStateInterface $form_state) {
    if ($form_state::hasAnyErrors()) {
      // When an error occurs during uploading files, remove all files so the
      // user can re-upload the files.
      $element['#value'] = [];
    }
    $values = $form_state->getValue('upload', []);
    if (!empty($values["url"])) {
      $dir = $element["#upload_location"];
      if (empty($dir)) {
        $dir = 'public://';
      }
      $file = self::getFileForUrl($values['url'], $dir);
      if (empty($file)) {
        return $element;
      }
      $element['#value']['fids'] = [
        $file->id(),
      ];
    }
    return $element;
  }

  /**
   * Helper function.
   */
  public static function getFileForUrl($url, $dir) {
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $http_client = \Drupal::httpClient();
    /** @var \Drupal\file\FileRepositoryInterface $file_repo */
    $file_repo = \Drupal::service('file.repository');
    return self::getFileForUrlInternal($url, $dir, $file_system, $http_client, $file_repo);
  }

  /**
   * Helper function.
   */
  protected static function getFileForUrlInternal($url, $dir, FileSystemInterface $file_system, ClientInterface $http_client, FileRepositoryInterface $file_repo) {
    // Get the actual contents, which will hopefully be an image, and will have
    // a content type.
    $extension = NULL;
    try {
      $response = $http_client->request('GET', $url);
      $content_type = $response->getHeaderLine('content-type');
      if (empty($content_type)) {
        return FALSE;
      }
      switch ($content_type) {
        case 'image/jpeg':
          $extension = 'jpg';
          break;

        case 'image/png':
          $extension = 'png';
          break;

        default:
          return FALSE;
      }
    }
    catch (\Throwable $e) {
      return FALSE;
    }
    if (!$extension || !$response) {
      return FALSE;
    }
    $data = (string) $response->getBody();
    if (!$data) {
      return FALSE;
    }
    $filename = md5($url);
    $file_system->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
    $filename = sprintf('%s/%s.%s', $dir, $filename, $extension);
    $file = $file_repo->writeData($data, $filename);
    if (!$file) {
      return FALSE;
    }
    $file->save();
    return $file;
  }

  /**
   * Submitter for the text field we have on file fields.
   */
  public static function submitText($form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    if (self::isRemoveButton($button)) {
      // The parent submitter will take care of that.
      return;
    }
    $parents = $button['#array_parents'];
    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($parents, 0, -1));
    $submitted_values = NestedArray::getValue($form_state->getValues(), array_slice($button['#parents'], 0, -2));
    foreach ($submitted_values as $delta => $value) {
      if (!empty($value['fids'])) {
        // We don't care about those. Other submitters must take care of this.
        continue;
      }
      if (empty($value['url'])) {
        // We don't care about those either.
        continue;
      }
      // Now let's try to download this image.
      $file = self::getFileForUrl($value['url'], $element["#upload_location"]);
      if (!$file) {
        continue;
      }
      $submitted_values[$delta]['fids'][] = $file->id();
    }
    NestedArray::setValue($form_state->getValues(), array_slice($button['#parents'], 0, -2), $submitted_values);
  }

}
