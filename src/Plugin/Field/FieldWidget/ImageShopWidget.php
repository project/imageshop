<?php

namespace Drupal\imageshop\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Element\ManagedFile;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Drupal\imageshop\Element\ImageShopElement;

/**
 * Defines the 'imageshop_widget' field widget.
 *
 * @FieldWidget(
 *   id = "imageshop_widget",
 *   label = @Translation("Imageshop widget"),
 *   field_types = {"image"},
 * )
 */
class ImageShopWidget extends ImageWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $el = parent::formElement($items, $delta, $element, $form, $form_state);
    $el["#type"] = 'imageshop';
    // The grandparent class of this sets the process to the process of
    // managed_file. We want it to be imageshop.
    foreach ($el["#process"] as $delta => $process) {
      if (!is_array($process)) {
        continue;
      }
      if (empty($process[0])) {
        continue;
      }
      if ($process[0] !== ManagedFile::class) {
        continue;
      }
      $el["#process"][$delta][0] = ImageShopElement::class;
    }
    return $el;
  }

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $el = parent::process($element, $form_state, $form);
    return $el;
  }

}
