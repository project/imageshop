(function($) {

  function setSize() {
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
      var columns = parseInt(($("iframe").width() - 50) / 152);
      var rows = Math.ceil(50 / columns);
      var height = $(window).width() + rows * 298 + 100;

      $("iframe").height(height);
      $("iframe").addClass("safari");
      $("body").addClass("safari");
    } else {
      $("iframe").height($(window).height());
      $("iframe").width($(window).width());
    }
  }

  function listener(event) {
    var eventdata = event.data.split(";");
    insert(eventdata[0], eventdata[1], eventdata[2], eventdata[3]);
  }

  $(function () {

    var iframe = document.getElementById("MyFrame")
    var baseUrl = drupalSettings.imageShop.baseUrl;

    iframe.src = baseUrl + drupalSettings.imageShop.urlParams;

    $(window).resize(function () {
      setSize();
    });

    setSize();

    if (window.addEventListener) {
      addEventListener("message", listener, false);
    } else {
      attachEvent("onmessage", listener);
    }
  });

  function insert(file, title, width, height) {
    window.parent.opener.postMessage(file, '*');
    window.close();
  }


})(jQuery);
