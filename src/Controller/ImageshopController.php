<?php

namespace Drupal\imageshop\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\imageshop\Form\SettingsForm;
use Drupal\imageshop\TokenService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Imageshop routes.
 */
class ImageshopController extends ControllerBase {

  /**
   * Bare HTML renderer.
   *
   * @var \Drupal\Core\Render\BareHtmlPageRendererInterface
   */
  protected $bareHtmlRenderer;

  /**
   * Token service.
   *
   * @var \Drupal\imageshop\TokenService
   */
  protected $tokenService;

  /**
   * ImageshopController constructor.
   */
  public function __construct(BareHtmlPageRendererInterface $renderer, TokenService $tokenService) {
    $this->bareHtmlRenderer = $renderer;
    $this->tokenService = $tokenService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bare_html_page_renderer'),
      $container->get('imageshop.token_service')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $temp_token = $this->tokenService->getTemporaryToken();
    if (!$temp_token) {
      $render = [
        '#markup' => $this->t('Your imageshop settings seems to be not configured or faulty. Please check that you are connected with imageshop.'),
      ];
      return $this->bareHtmlRenderer->renderBarePage($render, 'Select image', 'markup', $render);
    }
    $settings = $this->config('imageshop.settings')->get('imageshop_browser_settings');
    $settings = SettingsForm::createBrowserSettings($settings);
    $url_params = sprintf('CULTURE=%s&SETDOMAIN=false&FREECROP=%s&SHOWSIZEDIALOGUE=%s&SHOWCROPDIALOGUE=%s&INSERTIMMEDIATELY=%s&IMAGESHOPINTERFACENAME=%s&IMAGESHOPSIZES=Normal 320x240;320x240:Stor 640x480;640x480&IMAGESHOPDOCUMENTPREFIX=SC-&IMAGESHOPTOKEN=%s',
      $settings['culture'],
      $settings['free_crop'],
      $settings['show_size_dialog'],
      $settings['show_crop_dialog'],
      $settings['insert_immediately'],
      $settings["interface_name"],
      $temp_token->getToken()
    );
    $render = [
      '#theme' => 'imageshop_iframe',
      '#attached' => [
        'library' => [
          'imageshop/imageshop_iframe',
        ],
        'drupalSettings' => [
          'imageShop' => [
            'baseUrl' => 'https://client.imageshop.no/InsertImage2.aspx?IFRAMEINSERT=true&',
            'urlParams' => $url_params,
          ],
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $this->moduleHandler()->alter('imageshop_iframe_render', $render);
    return $this->bareHtmlRenderer->renderBarePage($render, 'Select image', 'markup', $render);
  }

}
