<?php

namespace Drupal\imageshop;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * TokenService service to get temp tokens from tokens.
 */
class TokenService {

  const TOKEN_STATE_KEY = 'imageshop:token_state_key';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a TokenService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, StateInterface $state, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->state = $state;
    $this->logger = $logger;
  }

  /**
   * Method description.
   */
  public function getTemporaryToken() {
    /** @var \Drupal\imageshop\ImageShopTempToken $token */
    $token = $this->state->get(self::TOKEN_STATE_KEY, FALSE);
    // Check if this token is good.
    if ($token && !$token->isExpired()) {
      return $token;
    }
    return $this->regenerateToken();
  }

  /**
   * Regenerates the temp token.
   */
  public function regenerateToken() {
    try {
      $config = $this->configFactory->get('imageshop.settings');
      if (!$config->get('token')) {
        throw new \InvalidArgumentException('No token found in imageshop settings');
      }
      $url = sprintf('https://webservices.imageshop.no/V4.asmx/GetTemporaryToken?token=%s&privateKey=%s', $config->get('token'), $config->get('private_key'));
      $data = $this->httpClient->request('GET', $url);
      $xml = simplexml_load_string($data->getBody());
      $token = (string) $xml;
      if (empty($token)) {
        throw new \UnexpectedValueException('No token found in response');
      }
      $temp_token = new ImageShopTempToken($token);
      $this->state->set(self::TOKEN_STATE_KEY, $temp_token);
      return $temp_token;
    }
    catch (\Throwable $e) {
      $this->logger->error('Caught exception while trying to regenerate an imageshop token. The error was: @err and the stack trace was: @trace', [
        '@err' => $e->getMessage(),
        '@trace' => $e->getTraceAsString(),
      ]);
    }
    return FALSE;
  }

}
