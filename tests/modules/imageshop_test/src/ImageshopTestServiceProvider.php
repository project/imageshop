<?php

namespace Drupal\imageshop_test;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Defines a service provider for the Imageshop testing module.
 */
class ImageshopTestServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('imageshop.token_service')) {
      $def = $container->getDefinition('imageshop.token_service');
      $def->setClass(TestTokenService::class);
      $container->set('imageshop.token_service', $def);
    }
  }

}
