<?php

namespace Drupal\Tests\imageshop\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\media\Entity\MediaType;

/**
 * Test basic things with the element.
 *
 * @group imageshop
 */
class MediaBrowserTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'imageshop',
    'imageshop_test',
    'media_library',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalCreateContentType([
      'type' => 'article',
    ]);

    FieldStorageConfig::create([
      'field_name' => 'field_media',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'media',
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_media',
      'entity_type' => 'node',
      'bundle' => 'article',
    ])->save();
    $user = $this->drupalCreateUser([
      'access content',
      'access media overview',
      'edit own article content',
      'access imageshop',
      'create article content',
      'create media',
      'view media',
    ]);
    $this->drupalLogin($user);
    $this->container
      ->get('entity_display.repository')
      ->getFormDisplay('node', 'article')
      ->setComponent('field_media', [
        'type' => 'media_library_widget',
        'region' => 'content',
        'settings' => [
          'media_types' => ['image'],
        ],
      ])
      ->save();

    $media_type = MediaType::create([
      'source' => 'image',
      'id' => 'image',
      'name' => 'image',
    ]);
    $media_type->save();
    // Create the source field.
    $source_field = $media_type->getSource()->createSourceField($media_type);
    /** @var \Drupal\field\Entity\FieldStorageConfig $def */
    $def = $source_field->getFieldStorageDefinition();
    $def->save();
    $source_field->save();
    $media_type
      ->set('source_configuration', [
        'source_field' => $source_field->getName(),
      ])
      ->save();
  }

  /**
   * Test that media browser downloads a file when attached via a field.
   *
   * @dataProvider getConfigValues
   */
  public function testMediaBrowserDownloadsFileInField($value) {
    // We only change it if not default value.
    if (!$value) {
      $this->config('imageshop.settings')->set('hide_media_browser', $value)->save();
    }
    $this->drupalGet('/node/add/article');
    // Click the browse button of the media library.
    $field_name = 'field_media';
    $this->assertSession()->pageTextContains('No media items are selected.');
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $session */
    $session = $this->assertSession();
    $element = $session->waitForElement('css', "#$field_name-media-library-wrapper.js-media-library-widget");
    self::assertNotEmpty($element);
    $element->pressButton('Add media');
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $session */
    $session = $this->assertSession();
    $session->assertWaitOnAjaxRequest();
    $element = $session->waitForElement('css', '.imageshop-trigger');
    $files = $this->container->get('entity_type.manager')->getStorage('file')->loadByProperties([]);
    self::assertEmpty($files);
    $element->click();
    // Now we should have downloaded druplicon.png with the widget.
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $session */
    $session = $this->assertSession();
    $session->waitForElementRemoved('css', '.imageshop-trigger');
    $session->assertWaitOnAjaxRequest();
    // Click the save button.
    $this->getSession()->getPage()->find('css', '.ui-dialog-buttonpane .button--primary')->click();
    // Make sure JS tings has run.
    sleep(1);
    if (!$value) {
      /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $session */
      $session = $this->assertSession();
      $session->waitForElement('css', '#media-library-view img')->isVisible();
      // Now we actually need to click a save button.
      $this->getSession()->getPage()->find('css', '.ui-dialog-buttonpane .button--primary')->click();
    }
    // Then the thumbnail should be there.
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $session */
    $session = $this->assertSession();
    $session->waitForElement('css', '.media-library-selection img');
    // And this text should not.
    $session->pageTextNotContains('No media items are selected.');
    // Now we should have a file.
    $files = $this->container->get('entity_type.manager')->getStorage('file')->loadByProperties([]);
    self::assertNotEmpty($files);
  }

  /**
   * Test that things work even when our browser is attached.
   */
  public function testMediaNotAttached() {
    /** @var \Drupal\FunctionalJavascriptTests\WebDriverWebAssert $session */
    $session = $this->assertSession();
    // We only change it if not default value.
    $this->config('imageshop.settings')->set('enabled_media_types', [])->save();
    $this->drupalGet('/node/add/article');
    // Click the browse button of the media library.
    $field_name = 'field_media';
    $session->pageTextContains('No media items are selected.');
    $element = $session->waitForElement('css', "#$field_name-media-library-wrapper.js-media-library-widget");
    self::assertNotEmpty($element);
    $element->pressButton('Add media');
    self::assertNotNull($session->waitForText('Add or select media'));
    self::assertNull($session->waitForElement('css', '.imageshop-trigger'));
  }

  /**
   * A dataprovider.
   */
  public static function getConfigValues() {
    return [
      [FALSE],
      [TRUE],
    ];
  }

}
